package com.example.nguyenphamminhquang.sample.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.nguyenphamminhquang.sample.R
import com.example.nguyenphamminhquang.sample.utility.ImageUtility
import kotlinx.android.synthetic.main.layout_custom.view.*

class CustomView  @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr){
    init {
        LayoutInflater.from(context).inflate(R.layout.layout_custom,this)
    }

    var title: String? = ""
    set(value) {
        tvTitle.text = value
        field = value
    }

    var imageURL: String? = ""
    set(value) {
        field = value
        ImageUtility.loadImageFromURL(image, value)
    }
}