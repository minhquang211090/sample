package com.example.nguyenphamminhquang.sample.utility

import android.graphics.BitmapFactory
import android.util.Log
import android.widget.ImageView
import okhttp3.*
import java.io.BufferedInputStream
import java.io.IOException


class ImageUtility {
    companion object {
        val TAG = "ImageUtility"

        fun loadImageFromURL(imageView: ImageView, imageURL: String?){
            if (imageURL.isNullOrEmpty()) return
            //Todo: Handle save image cache to avoid out of memory
            val client = OkHttpClient()
            val request = Request.Builder().url(imageURL).get().build()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException?) {
                    Log.d(TAG, "Error " + e?.message)
                }

                override fun onResponse(call: Call?, response: Response?) {
                    val input = response?.body()
                    val inputStream = input?.byteStream()
                    Log.i("inputStream", "inputstream value = $inputStream")
                    val bufferedInputStream = BufferedInputStream(inputStream)
                    val bitmap = BitmapFactory.decodeStream(bufferedInputStream)
                    Log.i("bitmap", "bitmap value = $bitmap")
                    imageView.post { imageView.setImageBitmap(bitmap) }
                }

            })
        }
    }
}