package com.example.nguyenphamminhquang.sample.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nguyenphamminhquang.sample.R
import com.example.nguyenphamminhquang.sample.Repository
import com.example.nguyenphamminhquang.sample.model.MainData
import kotlinx.android.synthetic.main.item_main.view.*
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        initListData()
    }

    private fun initListData(){
        recyclerView.layoutManager = LinearLayoutManager(context)
        progressbar.visibility = View.VISIBLE
        viewModel.data.observe(this, Observer {
            recyclerView.adapter = MainAdapter(it)
            progressbar.visibility = View.INVISIBLE
        })
        viewModel.error.observe(this, Observer { error ->
            progressbar.visibility = View.INVISIBLE
            if (error.isNullOrEmpty()) Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
        })
    }

}

class MainAdapter(val listData: List<MainData>): RecyclerView.Adapter<MainViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.setData(listData[position])
    }

}

class MainViewHolder(parent: ViewGroup,
                     view: View = LayoutInflater.from(parent.context)
                             .inflate(R.layout.item_main,parent,false))
    :RecyclerView.ViewHolder(view){
    fun setData(data: MainData){
        itemView.view.imageURL = data.imageURL
        itemView.view.title = data.title
    }
}
