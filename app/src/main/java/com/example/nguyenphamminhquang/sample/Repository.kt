package com.example.nguyenphamminhquang.sample

import com.example.nguyenphamminhquang.sample.model.MainData
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

class Repository {

    fun getMainData(callBack: (List<MainData>, String) -> Unit) {
        val listData = ArrayList<MainData>()
        val client = OkHttpClient()
        val request = Request.Builder().url("http://jsonplaceholder.typicode.com/photos").build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callBack(listData, e.toString())
            }

            override fun onResponse(call: Call, response: Response) {
                if (response.isSuccessful) {
                    val jsonArray = JSONArray(response.body()?.string())
                    for (index in 0..100) {
                        val mainData = convertJsonToMainData(jsonArray.getJSONObject(index))
                        listData.add(mainData)
                    }
                    callBack(listData, "")
                }
            }

        })
    }

    private fun convertJsonToMainData(jsonObject: JSONObject): MainData {
        val title = jsonObject.optString("title")
        val thumbnailURL = jsonObject.optString("thumbnailUrl")
        return MainData(title, thumbnailURL)
    }
}