package com.example.nguyenphamminhquang.sample.ui.main

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nguyenphamminhquang.sample.Repository
import com.example.nguyenphamminhquang.sample.model.MainData

class MainViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    val respository = Repository()
    internal val data = MutableLiveData<List<MainData>>()
    internal val error = MutableLiveData<String>()
    init {
        respository.getMainData({ listData, er ->
            Handler(Looper.getMainLooper()).post {
                data.value = listData
                error.value = er
            }
        })
    }
}
